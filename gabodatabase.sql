--
-- PostgreSQL database dump
--

-- Dumped from database version 12.2 (Debian 12.2-2.pgdg100+1)
-- Dumped by pg_dump version 12.2 (Debian 12.2-2.pgdg100+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

ALTER TABLE ONLY violence_registry.vr_registry_state DROP CONSTRAINT pk_vr_registry_state;
ALTER TABLE ONLY violence_registry.vr_violence_registry DROP CONSTRAINT "PK_vr_violence_registry";
DROP TABLE violence_registry.vr_violence_registry;
DROP SEQUENCE violence_registry.vr_violence_registry_pk_seq;
DROP TABLE violence_registry.vr_registry_state;
DROP SCHEMA violence_registry;
--
-- Name: violence_registry; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA violence_registry;


ALTER SCHEMA violence_registry OWNER TO postgres;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: vr_registry_state; Type: TABLE; Schema: violence_registry; Owner: postgres
--

CREATE TABLE violence_registry.vr_registry_state (
    rees_id bigint NOT NULL,
    rees_name character varying(5) NOT NULL,
    rees_description character varying(50) NOT NULL,
    creationdate timestamp without time zone NOT NULL,
    creationuser character varying(50) NOT NULL,
    modificationdate timestamp without time zone,
    modificationuser character varying(50)
);


ALTER TABLE violence_registry.vr_registry_state OWNER TO postgres;

--
-- Name: vr_registry_state_rees_id_seq; Type: SEQUENCE; Schema: violence_registry; Owner: postgres
--

ALTER TABLE violence_registry.vr_registry_state ALTER COLUMN rees_id ADD GENERATED BY DEFAULT AS IDENTITY (
    SEQUENCE NAME violence_registry.vr_registry_state_rees_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: vr_violence_registry_pk_seq; Type: SEQUENCE; Schema: violence_registry; Owner: postgres
--

CREATE SEQUENCE violence_registry.vr_violence_registry_pk_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE violence_registry.vr_violence_registry_pk_seq OWNER TO postgres;

--
-- Name: vr_violence_registry; Type: TABLE; Schema: violence_registry; Owner: postgres
--

CREATE TABLE violence_registry.vr_violence_registry (
    vire_id bigint DEFAULT nextval('violence_registry.vr_violence_registry_pk_seq'::regclass) NOT NULL,
    vire_user_name character varying(50) NOT NULL,
    vire_user_login character varying(50) NOT NULL,
    vire_location_longitude character varying(50) NOT NULL,
    vire_location_latitude character varying(50) NOT NULL,
    vire_user_cellphone character varying(50) NOT NULL,
    vire_user_email character varying(50) NOT NULL,
    vire_state character varying(5) NOT NULL,
    vire_date timestamp without time zone NOT NULL,
    creationdate timestamp without time zone NOT NULL,
    creationuser character varying(50) NOT NULL,
    modificationdate timestamp without time zone,
    modificationuser character varying(50)
);


ALTER TABLE violence_registry.vr_violence_registry OWNER TO postgres;

--
-- Data for Name: vr_registry_state; Type: TABLE DATA; Schema: violence_registry; Owner: postgres
--

COPY violence_registry.vr_registry_state (rees_id, rees_name, rees_description, creationdate, creationuser, modificationdate, modificationuser) FROM stdin;
\.


--
-- Data for Name: vr_violence_registry; Type: TABLE DATA; Schema: violence_registry; Owner: postgres
--

COPY violence_registry.vr_violence_registry (vire_id, vire_user_name, vire_user_login, vire_location_longitude, vire_location_latitude, vire_user_cellphone, vire_user_email, vire_state, vire_date, creationdate, creationuser, modificationdate, modificationuser) FROM stdin;
\.


--
-- Name: vr_registry_state_rees_id_seq; Type: SEQUENCE SET; Schema: violence_registry; Owner: postgres
--

SELECT pg_catalog.setval('violence_registry.vr_registry_state_rees_id_seq', 1, false);


--
-- Name: vr_violence_registry_pk_seq; Type: SEQUENCE SET; Schema: violence_registry; Owner: postgres
--

SELECT pg_catalog.setval('violence_registry.vr_violence_registry_pk_seq', 1, false);


--
-- Name: vr_violence_registry PK_vr_violence_registry; Type: CONSTRAINT; Schema: violence_registry; Owner: postgres
--

ALTER TABLE ONLY violence_registry.vr_violence_registry
    ADD CONSTRAINT "PK_vr_violence_registry" PRIMARY KEY (vire_id);


--
-- Name: vr_registry_state pk_vr_registry_state; Type: CONSTRAINT; Schema: violence_registry; Owner: postgres
--

ALTER TABLE ONLY violence_registry.vr_registry_state
    ADD CONSTRAINT pk_vr_registry_state PRIMARY KEY (rees_id);


--
-- PostgreSQL database dump complete
--

