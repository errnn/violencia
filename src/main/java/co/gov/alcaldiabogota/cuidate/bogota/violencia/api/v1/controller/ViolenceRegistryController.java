package co.gov.alcaldiabogota.cuidate.bogota.violencia.api.v1.controller;


import java.util.Map;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;

import co.gov.alcaldiabogota.cuidate.bogota.violencia.api.v1.data.dto.ViolenceRegistryDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.SwaggerDefinition;
import io.swagger.annotations.Tag;

@Api(tags = { "Violence Registry" })
@SwaggerDefinition(tags = { @Tag(name = "Violence Registry Service", description = "Api for violence registry") })
@RequestMapping(ViolenceRegistryController.PATH_BASE)
public interface ViolenceRegistryController {
	
	public static final String PATH_BASE = "/api/v1/";

	@PostMapping("/violenceregistry/")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Violence reigstry is create"), @ApiResponse(code = 401, message = "401 Unauthorized"),
			@ApiResponse(code = 403, message = "403 Forbbiden"), @ApiResponse(code = 404, message = "404 No found") })
	ResponseEntity<ViolenceRegistryDTO> createViolenceRegistry(@RequestHeader Map<String, String> headers, @RequestBody ViolenceRegistryDTO violenceRegistry)throws Exception;


}
