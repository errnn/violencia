package co.gov.alcaldiabogota.cuidate.bogota.violencia.api.v1.data.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import co.gov.alcaldiabogota.cuidate.bogota.violencia.api.v1.data.model.ViolenceRegistry;

@Repository("violenceRegistryRepository")
public interface ViolenceRegistryRepository extends JpaRepository<ViolenceRegistry, Long> {

	
	
}