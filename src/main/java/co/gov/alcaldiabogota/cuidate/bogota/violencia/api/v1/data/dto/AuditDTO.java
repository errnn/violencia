package co.gov.alcaldiabogota.cuidate.bogota.violencia.api.v1.data.dto;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;


@Data
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
@ApiModel(description = "Audit information")
public class AuditDTO implements Serializable{

	private static final long serialVersionUID = 1L;
	private String user;

}
