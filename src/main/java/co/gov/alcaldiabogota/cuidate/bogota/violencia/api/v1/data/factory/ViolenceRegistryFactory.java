package co.gov.alcaldiabogota.cuidate.bogota.violencia.api.v1.data.factory;

import java.io.Serializable;

import co.gov.alcaldiabogota.cuidate.bogota.violencia.api.v1.data.dto.ViolenceRegistryDTO;
import co.gov.alcaldiabogota.cuidate.bogota.violencia.api.v1.data.model.ViolenceRegistry;

public class ViolenceRegistryFactory implements Serializable {

	private static final long serialVersionUID = 1L;

	public static ViolenceRegistry DTOToEntity(ViolenceRegistryDTO violenceRegistryDTO) {
		final ViolenceRegistry violenceRegistry = new ViolenceRegistry();
		if (violenceRegistryDTO == null) {
			return violenceRegistry;
		}

		violenceRegistry.setId(violenceRegistryDTO.getId());
		violenceRegistry.setUserName(violenceRegistryDTO.getUserName());
		violenceRegistry.setUserLogin(violenceRegistryDTO.getUserLogin());
		violenceRegistry.setUserEmail(violenceRegistryDTO.getUserEmail());
		violenceRegistry.setLocationLongitude(violenceRegistryDTO.getLocationLongitude());
		violenceRegistry.setLocationLatitude(violenceRegistryDTO.getLocationLatitude());
		violenceRegistry.setUserCellphone(violenceRegistryDTO.getUserCellphone());
		violenceRegistry.setState(violenceRegistryDTO.getState());
		violenceRegistry.setDate(violenceRegistryDTO.getDate());
		violenceRegistry.setCreationDate(violenceRegistryDTO.getCreationDate());
		violenceRegistry.setCreationUser(violenceRegistryDTO.getCreationUser());
		violenceRegistry.setModificationUser(violenceRegistryDTO.getModificationUser());
		violenceRegistry.setModificationDate(violenceRegistryDTO.getModificationDate());

		return violenceRegistry;

	}

	public static ViolenceRegistryDTO EntityToDTO(ViolenceRegistry violenceRegistry) {
		final ViolenceRegistryDTO violenceRegistryDTO = new ViolenceRegistryDTO();
		if (violenceRegistry == null) {
			return violenceRegistryDTO;
		}

		violenceRegistryDTO.setId(violenceRegistry.getId());
		violenceRegistryDTO.setUserName(violenceRegistry.getUserName());
		violenceRegistryDTO.setUserLogin(violenceRegistry.getUserLogin());
		violenceRegistryDTO.setUserEmail(violenceRegistry.getUserEmail());
		violenceRegistryDTO.setLocationLongitude(violenceRegistry.getLocationLongitude());
		violenceRegistryDTO.setLocationLatitude(violenceRegistry.getLocationLatitude());
		violenceRegistryDTO.setUserCellphone(violenceRegistry.getUserCellphone());
		violenceRegistryDTO.setState(violenceRegistry.getState());
		violenceRegistryDTO.setDate(violenceRegistry.getDate());
		violenceRegistryDTO.setCreationDate(violenceRegistry.getCreationDate());
		violenceRegistryDTO.setCreationUser(violenceRegistry.getCreationUser());
		violenceRegistryDTO.setModificationUser(violenceRegistry.getModificationUser());
		violenceRegistryDTO.setModificationDate(violenceRegistry.getModificationDate());

		return violenceRegistryDTO;

	}

}
