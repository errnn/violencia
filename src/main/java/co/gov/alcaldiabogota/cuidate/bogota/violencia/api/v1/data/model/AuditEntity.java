package co.gov.alcaldiabogota.cuidate.bogota.violencia.api.v1.data.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import lombok.Data;

@Data
@MappedSuperclass
public abstract class AuditEntity implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Temporal(value = TemporalType.TIMESTAMP)
	@CreationTimestamp
	@Column(name = "CREATIONDATE", insertable = true, updatable = false)
	protected Date creationDate;

	@Column(name = "CREATIONUSER", insertable = true, updatable = false)
	protected String creationUser;

	@Temporal(value = TemporalType.TIMESTAMP)
	@UpdateTimestamp
	@Column(name = "MODIFICATIONDATE", insertable = false, updatable = true)
	protected Date modificationDate;

	@Column(name = "MODIFICATIONUSER", insertable = false, updatable = true)
	protected String modificationUser;
	
}