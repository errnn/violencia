package co.gov.alcaldiabogota.cuidate.bogota.violencia.api.v1.data.dto;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
@ApiModel(description = "Violence registry information")
public class ViolenceRegistryDTO extends AuditEntityDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;
	private String userName;
	private String userLogin;
	private String userEmail;
	private String locationLongitude;
	private String locationLatitude;
	private String userCellphone;
	private String state;
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", timezone = "America/Bogota")
	protected Date date;
	
	

}
