package co.gov.alcaldiabogota.cuidate.bogota.violencia.api.v1.controller.impl;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import co.gov.alcaldiabogota.cuidate.bogota.violencia.api.v1.controller.ViolenceRegistryController;
import co.gov.alcaldiabogota.cuidate.bogota.violencia.api.v1.data.dto.AuditDTO;
import co.gov.alcaldiabogota.cuidate.bogota.violencia.api.v1.data.dto.ViolenceRegistryDTO;
import co.gov.alcaldiabogota.cuidate.bogota.violencia.api.v1.data.service.ViolenceRegistryService;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping(ViolenceRegistryRestControllerImpl.PATH_BASE)
public class ViolenceRegistryRestControllerImpl implements ViolenceRegistryController{
	public static final String PATH_BASE = "/api/v1";



	private final Logger logger = LoggerFactory.getLogger(ViolenceRegistryRestControllerImpl.class);
	
	@Autowired
	private ViolenceRegistryService violenceRegistryService;
	
	@Override
	public ResponseEntity<ViolenceRegistryDTO> createViolenceRegistry(Map<String, String> headers, ViolenceRegistryDTO violenceRegistryDTO) throws Exception{
		logger.info("Init createViolenceRegistry violenceRegistry={}",violenceRegistryDTO);
		AuditDTO auditDTO = new AuditDTO();
		auditDTO.setUser(headers.get("user"));
		return new ResponseEntity<ViolenceRegistryDTO>(violenceRegistryService.createViolenceRegistry(auditDTO , violenceRegistryDTO), HttpStatus.OK);
	}
	@GetMapping("/violenceregistry/")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Violence list"), @ApiResponse(code = 401, message = "401 Unauthorized"),
			@ApiResponse(code = 403, message = "403 Forbbiden"), @ApiResponse(code = 404, message = "404 No found") })
	public ResponseEntity<ViolenceRegistryDTO> createViolenceRegistry(){
		return new ResponseEntity<ViolenceRegistryDTO>(new ViolenceRegistryDTO(), HttpStatus.OK);
	}

}
