package co.gov.alcaldiabogota.cuidate.bogota.violencia.api.v1.data.enums;

import java.util.Arrays;
import java.util.Optional;

import io.swagger.annotations.ApiModel;

@ApiModel(description = "Operation Enums")
public enum OperationEnum {
	CREATED("C"), UPDATE("U"), DELETE("D"), ERROR("E"), BAD_REQUEST("B"), UNAUTHORIZED("U"), NO_FOUND("N");

	public static OperationEnum[] ENUM_VALUES = OperationEnum.values();

	private final String value;

	private OperationEnum(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}

	public static OperationEnum get(final String value) {
		Optional<OperationEnum> optStudent = Arrays.stream(ENUM_VALUES).filter(s -> s.getValue().equals(value)).findFirst();
		return optStudent.isPresent() ? optStudent.get() : null;
	}

}