package co.gov.alcaldiabogota.cuidate.bogota.violencia.api.v1.data.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import co.gov.alcaldiabogota.cuidate.bogota.violencia.api.v1.data.dto.AuditDTO;
import co.gov.alcaldiabogota.cuidate.bogota.violencia.api.v1.data.dto.ViolenceRegistryDTO;
import co.gov.alcaldiabogota.cuidate.bogota.violencia.api.v1.data.enums.OperationEnum;
import co.gov.alcaldiabogota.cuidate.bogota.violencia.api.v1.data.exception.ViolenceReportException;
import co.gov.alcaldiabogota.cuidate.bogota.violencia.api.v1.data.factory.ViolenceRegistryFactory;
import co.gov.alcaldiabogota.cuidate.bogota.violencia.api.v1.data.model.ViolenceRegistry;
import co.gov.alcaldiabogota.cuidate.bogota.violencia.api.v1.data.repository.ViolenceRegistryRepository;
import co.gov.alcaldiabogota.cuidate.bogota.violencia.api.v1.data.service.ViolenceRegistryService;

@Service
public class ViolenceRegistryServiceImpl implements ViolenceRegistryService {

	private static final long serialVersionUID = 1L;
	@Autowired
	private ViolenceRegistryRepository violenceRegistryRepository;

	@Override
	public ViolenceRegistryDTO createViolenceRegistry(AuditDTO auditDTO, ViolenceRegistryDTO violenceRegistryDTO)
			throws ViolenceReportException {
		createViolenceRegistryValidations(auditDTO, violenceRegistryDTO);
		try {
			ViolenceRegistry violenceRegistry= ViolenceRegistryFactory.DTOToEntity(violenceRegistryDTO);
			violenceRegistry.setCreationUser(auditDTO.getUser());

			ViolenceRegistryDTO violenceRegistryDTOCreated = ViolenceRegistryFactory.EntityToDTO(violenceRegistryRepository.save(violenceRegistry));
			return violenceRegistryDTOCreated;
			
		} catch (Exception e) {
			e.printStackTrace();
			throw new ViolenceReportException(e.getMessage(),e.getCause());
		}

	}
	private void createViolenceRegistryValidations(AuditDTO auditDTO, ViolenceRegistryDTO violenceRegistryDTO) throws ViolenceReportException {
		if (auditDTO == null || auditDTO.getUser() == null || violenceRegistryDTO == null  
				|| violenceRegistryDTO.getLocationLatitude()== null || violenceRegistryDTO.getLocationLongitude() == null
				|| violenceRegistryDTO.getUserCellphone()== null || violenceRegistryDTO.getUserName()== null
				|| violenceRegistryDTO.getUserLogin()== null)
			getException("BAD_REQUEST", OperationEnum.BAD_REQUEST.getValue());

	}
	private void getException(String message, String cause) throws ViolenceReportException {
		Throwable throwable = new Throwable(cause);
		ViolenceReportException e = new ViolenceReportException("Violence registry service problem: "+message, throwable);
		throw e;
	}
}
