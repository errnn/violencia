package co.gov.alcaldiabogota.cuidate.bogota.violencia.api.v1.data.service;

import java.io.Serializable;

import co.gov.alcaldiabogota.cuidate.bogota.violencia.api.v1.data.dto.AuditDTO;
import co.gov.alcaldiabogota.cuidate.bogota.violencia.api.v1.data.dto.ViolenceRegistryDTO;
import co.gov.alcaldiabogota.cuidate.bogota.violencia.api.v1.data.exception.ViolenceReportException;

public interface ViolenceRegistryService extends Serializable{

	ViolenceRegistryDTO createViolenceRegistry(AuditDTO auditDTO, ViolenceRegistryDTO violenceRegistryDTO) throws ViolenceReportException;
	

}
