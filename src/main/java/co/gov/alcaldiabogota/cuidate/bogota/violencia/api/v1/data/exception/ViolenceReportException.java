package co.gov.alcaldiabogota.cuidate.bogota.violencia.api.v1.data.exception;

public class ViolenceReportException extends Exception {

	private static final long serialVersionUID = 1L;

	public ViolenceReportException(Throwable throwable) {
		super(throwable);
	}

	public ViolenceReportException(String message) {
		super(message);
	}

	public ViolenceReportException(String message, Throwable throwable) {
		super(message, throwable);
	}
}
