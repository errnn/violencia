package co.gov.alcaldiabogota.cuidate.bogota.violencia;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CuidateBogotaReporteViolenciaServicesApplication {

	public static void main(String[] args) {
		SpringApplication.run(CuidateBogotaReporteViolenciaServicesApplication.class, args);
	}

}
