package co.gov.alcaldiabogota.cuidate.bogota.violencia.api.v1.data.dto;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnore;

import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
@ApiModel(description = "AutiEntity")
public abstract class AuditEntityDTO implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	@JsonIgnore
	//@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", timezone = "America/Bogota")
	protected Date creationDate;
	@JsonIgnore
	protected String creationUser;
	@JsonIgnore
	//@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", timezone = "America/Bogota")
	protected Date modificationDate;
	@JsonIgnore
	protected String modificationUser;
	
}
