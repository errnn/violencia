package co.gov.alcaldiabogota.cuidate.bogota.violencia.api.v1.data.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@Entity
@Table(name = "vr_violence_registry")
public class ViolenceRegistry extends AuditEntity {
	private static final long serialVersionUID = 1L;

	@GenericGenerator(name = "violenceRegistryIdGenerator", strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator", parameters = {
			@Parameter(name = "sequence_name", value = "vr_violence_registry_pk_seq"), })
	
	@Id
	@GeneratedValue(generator = "violenceRegistryIdGenerator")
	@Column(name = "vire_id", nullable = false)
	private Long id;
	@Column(name = "vire_user_name", nullable = false)
	private String userName;
	@Column(name = "vire_user_login", nullable = false)
	private String userLogin;
	@Column(name = "vire_user_email", nullable = false)
	private String userEmail;
	@Column(name = "vire_location_longitude", nullable = false)
	private String locationLongitude;
	@Column(name = "vire_location_latitude", nullable = false)
	private String locationLatitude;
	@Column(name = "vire_user_cellphone", nullable = false)
	private String userCellphone;
	
	@Column(name = "vire_state", nullable = false)
	private String state;
	@Temporal(value = TemporalType.TIMESTAMP)
	@CreationTimestamp
	@Column(name = "vire_date", insertable = true, updatable = false)
	protected Date date;

}
