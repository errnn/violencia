FROM java:8
ADD target/cuidate-bogota-reporte-violencia-services-0.0.1-SNAPSHOT.jar /
RUN mkdir /log
RUN chmod -R 777 /log 
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/cuidate-bogota-reporte-violencia-services-0.0.1-SNAPSHOT.jar"]